#ifndef MOISTURE_H
#define MOISTURE_H

class SEN0193 
{
private:
    int MOISTURE_PIN, MAX_MOISTURE, MIN_MOISTURE;
    bool hasSetRangeValues();
    void getRangeValuesFromMemory();
    void setRangeValuesInMemory();
    void initialiseMoistureLevelRange();
    
public:
    SEN0193(int moisture_pin);
    void begin();
    float getReading();
};

#endif // MOISTURE_H