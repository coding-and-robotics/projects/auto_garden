#include "moisture.h"

#include <Arduino.h>
#include <EEPROM.h>

SEN0193::SEN0193(int moisturePin) : MOISTURE_PIN(moisturePin)
{

}

/**
 * @return true if moisture_values flag is present in EEPROM
 */
bool SEN0193::hasSetRangeValues() 
{
    String check;
    EEPROM.get(0, check);

    if (check == "moisture_values") 
        return true;
    
    return false;
}

/**
 * Retrieves the range values from memory and stores them in the private class variables
 */
void SEN0193::getRangeValuesFromMemory()
{
    String check;
    int eeAddr, minVal, maxVal;
    eeAddr = 0;

    EEPROM.get(eeAddr, check);
    eeAddr += sizeof(check);
    EEPROM.get(eeAddr, minVal);
    eeAddr += sizeof(int);
    EEPROM.get(eeAddr, maxVal);

    MIN_MOISTURE = minVal;
    MAX_MOISTURE = maxVal;
}

/**
 * Sets a flag to indicate range values have been written to memory, and then writes the range values to the Arduino EEPROM
 */
void SEN0193::setRangeValuesInMemory()
{
    String check = "moisture_values";
    int eeAddr = 0;
    EEPROM.put(eeAddr, check);
    eeAddr += sizeof(check);
    EEPROM.put(eeAddr, MIN_MOISTURE);
    eeAddr += sizeof(int);
    EEPROM.put(eeAddr, MAX_MOISTURE);
}

/**
 * Sets minimum and maximum values for the moisture level sensor. Allows us to normalise the reading into a percentage between those two values. The direct readings from the sensor are a little bizarre - the less moisture, the higher the reading, starting in the 5-600's and going down. Personally, I think a reading between 0 and 100, with an increase in moisture corresponding to a higher reading value, makes more sense.
 * 
 * Once the values are set, saves them to the Arduino EEPROM
 * 
 * @see getMoistureReading()
 * @see setRangeValuesInMemory()
 */
void SEN0193::initialiseMoistureLevelRange()
{
    int reading = 0;
    long startTime, countTime, currentTime;

    MIN_MOISTURE = 0;
    Serial.print("Please expose moisture sensor to the air for 5 seconds. ");
    Serial.print("Press <ENTER> when ready:");
    while(Serial.read() != '\n');
    startTime = countTime = millis();
    while ((currentTime = millis()) - startTime < 5000) 
    {
        if ((reading = analogRead(MOISTURE_PIN)) > MIN_MOISTURE) 
            MIN_MOISTURE = reading;
        if (currentTime - countTime > 1000) 
        {
            countTime = currentTime;
            Serial.print('.');
        }
    }
    Serial.println("Minimum moisture level calibrated");

    // minMoisture should now be the highest value we will see, so it is the 
    // best starting value for maxMoisture
    MAX_MOISTURE = MIN_MOISTURE;
    Serial.print("Please place moisture sensor in a cup of water for 5 ");
    Serial.print("seconds.Do not go\nbeyond the marked line (ideally 1-2cm ");
    Serial.print("below). Press <ENTER> when ready:");
    while(Serial.read() != '\n')
        ;
    startTime = countTime = millis();
    while((currentTime = millis()) - startTime < 5000) 
    {
        if ((reading = analogRead(MOISTURE_PIN)) < MAX_MOISTURE) 
            MAX_MOISTURE = reading;
        if(currentTime - countTime > 1000) 
        {
            countTime = currentTime;
            Serial.print('.');
        }
    }

    

    Serial.println("Maximum moisture level calibrated, calibration complete.");

    setRangeValuesInMemory();
    Serial.println("Details saved to EEPROM\n");
}

/**
 * Whilst it doesn't really need a begin() function, as it is just reading from an analogue pin, this allows us to do some runtime initialisation. Checks if there are any stored range values in the Arduino memory and retrieves them if so, or prompts user to initialise the ranges if not.
 */
void SEN0193::begin()
{
    if (hasSetRangeValues())
        getRangeValuesFromMemory();
    else
        initialiseMoistureLevelRange();
}

/**
 * @return a calibrated moisture level reading between 0.0 and 100.0
 */
float SEN0193::getReading() 
{
    return map(analogRead(MOISTURE_PIN), MIN_MOISTURE, MAX_MOISTURE, 0, 1000) / 10.0;
}