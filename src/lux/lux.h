#ifndef LUX_H
#define LUX_H

class TEMT6000 
{
private:
    int LUX_PIN;
public:
    TEMT6000(int luxPin);
    int getReading();
}

#endif // LUX_H