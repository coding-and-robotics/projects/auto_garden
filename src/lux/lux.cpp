#include "lux.h"
#include <Arduino.h>

TEMT6000::TEMT6000(int luxPin) : LUX_PIN(luxPin)
{

}

int TEMT6000::getReading()
{
    return (int)(analogRead(LUX_PIN) * 1000.0 / 1024.0);

    /*
     * https://forum.arduino.cc/index.php?topic=185158.0
     * lux = 2 * microamps
     * microamps = 1_000_000 * amps
     * amps = voltage / 10_000 (10k resistor on component)
     * voltage = reading * 5 / 1024
     */
}