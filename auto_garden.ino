#include <DHT.h>
// Wire.h is used in DHT.h
#include <Wire.h>

#include "src/moisture/moisture.h"
#include "src/lux/lux.h"

#define SERIAL_WAIT

#define BAUD        115200
#define DELAY       1000

#define DHTTYPE     DHT22
#define DHTPIN      11      // PWM pin
#define MOISTUREPIN 0       // Analogue pin
#define LUXPIN      1       // Analogue pin

// Sensors
DHT         dht(DHTPIN, DHTTYPE);
SEN0193     moisture(MOISTUREPIN);
TEMT6000    lux(LUXPIN);

void setup() {
    Serial.begin(BAUD);
  #ifdef SERIAL_WAIT
    while(!Serial);
  #endif
    Wire.begin();
    dht.begin();
    moisture.begin();
}

void loop() {
    float humidityAndTemperatureValues[2] = {0};
    int dhtRead = dht.readTempAndHumidity(humidityAndTemperatureValues);

    if (!dhtRead) {
        Serial.print("humidity: ");
        Serial.print(humidityAndTemperatureValues[0]);
        Serial.print("%\ttemperature: ");
        Serial.print(humidityAndTemperatureValues[1]);
        Serial.print("*C\t");
    }

    Serial.print("moisture: ");
    Serial.print(moisture.getReading(),1);
    Serial.print("%\t");

    Serial.print("lux: ");
    Serial.print(lux.getReading());
    Serial.println("lx");
  
    delay(DELAY);
}
