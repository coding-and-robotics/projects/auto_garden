# Automated Garden Project

Repository for the automated gardening project for the University of Liverpool Coding and Robotics Society.

Using an Arduino Uno as the brain (at least to start with)

## TODO

* add button (and LEDs)
    * add calibration mode
* add sensor to track water level in container
* add wifi/rf

## Sensors

*  DHT22/AM2302 - Temperature & Humidity Sensor (Grove)
   *  [seeed wiki](http://wiki.seeedstudio.com/Grove-Temperature_and_Humidity_Sensor_Pro/)
   *  [github](https://github.com/Seeed-Studio/Grove_Temperature_And_Humidity_Sensor)
*  SEN0193 - Capacitive Soil Moisture Sensor (DFRobot)
   *  [datasheet](https://media.digikey.com/pdf/Data%20Sheets/DFRobot%20PDFs/SEN0193_Web.pdf)
*  TEMT6000 - Lux sensor (keyestudio)
   *  [keyestudio wiki](https://wiki.keyestudio.com/Ks0098_keyestudio_TEMT6000_Ambient_Light_Sensor)
   *  Reads a value from 0-1023, 0 being very dark, 1023 being extremely bright
   *  Does not handle IR or UV well, nor poor lighting conditions

## Other Sensor Options

* SHT35 - Temperature & Humidity Sensor
    * [seeed wiki](http://wiki.seeedstudio.com/Grove-I2C_High_Accuracy_Temp&Humi_Sensor-SHT35/)
    * more expensive option
    * more accurate and better range
    * transmits via i2c
    * not really necessary for what we're doing(?)